// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "UObject/Object.h"
#include "Curves/CurveFloat.h"
#include "SenseGloveForceFeedback.generated.h"

class USenseGloveForceFeedback;

UENUM(BlueprintType)
enum class SenseGloveForceFeedbackChannel: uint8
{
	THUMB = 0,
	INDEX,
	MIDDLE,
	RING,
	PINKY,
	TOTAL
};

USTRUCT()
struct FSenseGloveForceFeedBackActuator
{
	GENERATED_BODY()

		FSenseGloveForceFeedBackActuator()
		: channel(0)
		, currentValue(0)
		, duration(.0f)
		, isLooping(0)
		, bNeedsUpdate(false)
	{}

	FSenseGloveForceFeedBackActuator(TEnumAsByte<SenseGloveForceFeedbackChannel> ch, int cur, float dur, bool loop, bool nu)
		: channel(ch)
		, currentValue(cur)
		, duration(dur)
		, isLooping(loop)
		, bNeedsUpdate(nu)
	{}

	TEnumAsByte<SenseGloveForceFeedbackChannel> channel;
	
	int currentValue;

	float duration;
	
	bool bNeedsUpdate;

	bool isLooping;

	bool Update(float DeltaTime);
};

USTRUCT(BlueprintType)
struct FSenseGloveForceFeedbackVal
{

	GENERATED_BODY()

	float thumb;
	float index;
	float middle;
	float ring;
	float pinky;

	FSenseGloveForceFeedbackVal()
		: thumb(0.f)
		, index(0.f)
		, middle(0.f)
		, ring(0.f)
		, pinky(0.f)
	{ }
};

/* This structure is are mainly used from blueprints 
to set ff params */
USTRUCT(BlueprintType)
struct FSenseGloveForceFeedbackParams
{
	GENERATED_BODY()

		FSenseGloveForceFeedbackParams()
		: bLooping(false)
		, channel(0)
		, duration(.0f)
		, intensity(0)
	{}

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "SenseGlove")
		TEnumAsByte<SenseGloveForceFeedbackChannel> channel;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "SenseGlove")
		bool bLooping;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "SenseGlove")
		float duration;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "SenseGlove")
		float intensity;

};


/**
 * A predefined force-feedback effect to be played on a controller
 */
UCLASS(BlueprintType, EditInlineNew)
class USenseGloveForceFeedback : public UObject
{
	GENERATED_UCLASS_BODY()

		//UPROPERTY(EditAnywhere, Category = "ForceFeedbackEffect")
		//TArray<FForceFeedbackChannelDetails> ChannelDetails;

	/** Duration of force feedback pattern in seconds. */
	UPROPERTY(Category = Info, AssetRegistrySearchable, VisibleAnywhere)
		float Duration;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float Duration2;

#if WITH_EDITOR
	virtual void PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent) override;
#endif

	float GetDuration();

	void GetValues() const;
};

