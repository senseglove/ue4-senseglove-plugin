/* robin@senseglove.com */

#pragma once

#include "CoreMinimal.h"

#include "Animation/AnimInstance.h"
#include "Animation/AnimInstanceProxy.h" 
#include "SenseGloveController.h"

#include "../Thirdparty/SenseGlove/incl/SenseGlove.h"
#include "../Thirdparty/SenseGlove/incl/DeviceList.h"
#include "../Thirdparty/SenseGlove/incl/Values.h"

#include "SenseGloveAnimInstance.generated.h"


/* Since Unreal 4.12 Animation is done in theads and should be called through 
this AnimInstanceProxy */
USTRUCT(BlueprintInternalUseOnly)
struct FSGProxy : public FAnimInstanceProxy
{
    GENERATED_BODY()

public:
    FSGProxy() : FAnimInstanceProxy() {}
    FSGProxy(UAnimInstance* Instance) : FAnimInstanceProxy(Instance) {}

    virtual void Update(float DeltaSeconds) override;
    virtual bool Evaluate(FPoseContext& Output) override;

    ASenseGloveController* Controller;


    int t1, t2, t3;
    int i1, i2, i3;
    int m1, m2, m3;
    int r1, r2, r3;
    int p1, p2, p3;

private:
    /* Local variables to retreive and store finger and thumb rotation */
    std::vector<std::vector<SGCore::Kinematics::Vect3D>> handAngles;
    SGCore::Kinematics::Vect3D hai1, hai2, hai3;
    SGCore::Kinematics::Vect3D ham1, ham2, ham3;
    SGCore::Kinematics::Vect3D har1, har2, har3;
    SGCore::Kinematics::Vect3D hap1, hap2, hap3;
    SGCore::Kinematics::Vect3D hat1, hat2, hat3;

};


UCLASS(Blueprintable)
class USenseGloveAnimInstance : public UAnimInstance
{
    GENERATED_BODY()

    FSGProxy Proxy;

    virtual FAnimInstanceProxy* CreateAnimInstanceProxy() override
    {
        // override this to just return the proxy on this instance
        return &Proxy;
    }

    virtual void DestroyAnimInstanceProxy(FAnimInstanceProxy* InProxy) override
    { }

    virtual void NativeInitializeAnimation() override;
    virtual void NativeUninitializeAnimation() override;

};

