/* robin@senseglove.com */

#include "SenseGloveController.h"
#include "SenseGloveAnimInstance.h"
#include "SenseGloveCollider.h"
#include "Engine.h"

ASenseGloveController::ASenseGloveController()
{
	PrimaryActorTick.bCanEverTick = true;

	for (int i = 0; i < maxFFChannels; ++i)
		ffActuators[i].channel = (TEnumAsByte<SenseGloveHapticChannel>) i;

	for (int i = 0; i < maxVibroChannels; ++i)
		vibroActuators[i].channel = (TEnumAsByte<SenseGloveHapticChannel>) i;

	meshContainer = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("GloveMesh"));

	RootComponent = meshContainer;

	thumbGrabArea = CreateDefaultSubobject<UBoxComponent>(TEXT("ThumbGrabArea"));
	thumbGrabArea->SetBoxExtent(FVector(.2f, .2f, .2f));
	indexGrabArea = CreateDefaultSubobject<UBoxComponent>(TEXT("IndexGrabArea"));
	indexGrabArea->SetBoxExtent(FVector(.2f, .2f, .2f));
	
	thumbCapsule = CreateDefaultSubobject<USenseGloveCollider>(TEXT("ThumbCollider"));
	indexCapsule = CreateDefaultSubobject<USenseGloveCollider>(TEXT("IndexCollider"));
	middleCapsule = CreateDefaultSubobject<USenseGloveCollider>(TEXT("MiddleCollider"));
	ringCapsule = CreateDefaultSubobject<USenseGloveCollider>(TEXT("RingCollider"));

	initializeMeshContainer();
	initializePhysics();
	initializeGrabbing();
	
	releaseObject.BindUObject(this, &ASenseGloveController::releaseObjects);

}

/* This will allow that changes are immediately visible in editor */
#if WITH_EDITOR
void ASenseGloveController::PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent)
{
	if (gloveType == GloveType::LeftGlove) {
		rightHanded = false;
		SGCore::HapticGlove::GetGlove(false, controllerHandleNew);
		UE_LOG(SenseGlove, Warning, TEXT("Left Glove Switch"));
		handProfile = SGCore::HandProfile::Default(false);
	}
	else if (gloveType == GloveType::RightGlove) {
		rightHanded = true;
		SGCore::HapticGlove::GetGlove(true, controllerHandleNew);
		UE_LOG(SenseGlove, Warning, TEXT("Right Glove Switch"));
		handProfile = SGCore::HandProfile::Default(true);
	}
	else {
		UE_LOG(SenseGlove, Warning, TEXT("Unknown Glove Type (neither right nor left"));
		return;
	}

}
#endif

void ASenseGloveController::PostInitializeComponents() {
	Super::PostInitializeComponents();
	if (SGCore::DeviceList::SenseCommRunning()) {
		/* Check GloveType instance set in Blueprint: allows live
		switching in editor */
		if (gloveType == GloveType::LeftGlove) {
			rightHanded = false;
			SGCore::HapticGlove::GetGlove(false, controllerHandleNew);
			UE_LOG(SenseGlove, Warning, TEXT("Left Glove Init"));
			handProfile = SGCore::HandProfile::Default(false);
		}
		else if (gloveType == GloveType::RightGlove) {
			rightHanded = true;
			SGCore::HapticGlove::GetGlove(true, controllerHandleNew);
			UE_LOG(SenseGlove, Warning, TEXT("Right Glove Init"));
			handProfile = SGCore::HandProfile::Default(true);
		}
		else {
			UE_LOG(SenseGlove, Warning, TEXT("Unknown Glove Type (neither right nor left"));
			return;
		}
	
	} else {
		//UE_LOG(SenseGlove, Log, TEXT("SenseComm is not Running (init routine)"));
	}

	initializeMeshContainer();

	if(!bDefaultPhysics) 
		deInitializePhysics();

	if(!bDefaultGrabbing)
		deInitializeGrabbing();
			
	if (!gloveAnimation) {
		gloveAnimation = USenseGloveAnimInstance::StaticClass();
	}

}

void ASenseGloveController::BeginPlay()
{
	Super::BeginPlay();
}

void ASenseGloveController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	bool bVibroNeedsUpdate = false;
	bool bForceFeedbackNeedsUpdate = false;

	for (int i = 0; i < (int) SenseGloveHapticChannel::TOTAL; ++i) {
		if (ffActuators[i].Update(DeltaTime)) {
			bForceFeedbackNeedsUpdate = true;
		}
		if (vibroActuators[i].Update(DeltaTime)) {
			bVibroNeedsUpdate = true;
		}
	}

	if (bForceFeedbackNeedsUpdate) {
		sendForceFeedbackRaw(ffActuators[0].currentValue, ffActuators[1].currentValue,
			ffActuators[2].currentValue, ffActuators[3].currentValue,
			ffActuators[4].currentValue);
	}

	if (bVibroNeedsUpdate) {
		sendBuzzRaw(vibroActuators[0].currentValue, vibroActuators[1].currentValue,
			vibroActuators[2].currentValue, vibroActuators[3].currentValue,
			vibroActuators[4].currentValue, vibroActuators[5].currentValue);
	}

	if (bNeedsRelease) {
		releaseObjects();
		bNeedsRelease = false;
	}

}

bool ASenseGloveController::sendBuzzRaw(int thumb, int index, int middle, int ring, int pinky, int wrist)

{
	/* Values should be between 0 - 100, no range checking because they are
	already checked in SenseComm */

	if (!SGCore::DeviceList::SenseCommRunning()) {
		UE_LOG(SenseGlove, Warning, TEXT("SenseComm is not running"));
		return false;
	}

	if (!controllerHandleNew)
		return false;

	buzz[0] = thumb;
	buzz[1] = index;
	buzz[2] = middle;
	buzz[3] = ring;
	buzz[4] = pinky;
	buzz[5] = wrist;

	controllerHandleNew->SendHaptics(SGCore::Haptics::SG_BuzzCmd(thumb, index, middle, ring, pinky));
	controllerHandleNew->SendHaptics(SGCore::Haptics::SG_FFBCmd(ff[0], ff[1], ff[2], ff[3], ff[4]));
	
	if (controllerHandleNew->GetDeviceType() == SGCore::DeviceType::NOVA) {	
		controllerHandleNew->SendHaptics(SGCore::Haptics::ThumperCmd(buzz[5]));
	}

	return true;
}

bool ASenseGloveController::sendForceFeedbackRaw(int thumb, int index, int middle, int ring, int pinky)
{
	/* Values should be between 0 - 100, no range checking because they are
	already checked in SenseComm */

	if (!SGCore::DeviceList::SenseCommRunning()) {
		UE_LOG(SenseGlove, Warning, TEXT("SenseComm is not running"));
		return false;
	}

	if (!controllerHandleNew)
		return false;

	ff[0] = thumb;
	ff[1] = index;
	ff[2] = middle;
	ff[3] = ring;
	ff[4] = pinky;

	controllerHandleNew->SendHaptics(SGCore::Haptics::SG_BuzzCmd(buzz[0], buzz[1], buzz[2], buzz[3], buzz[4]));
	controllerHandleNew->SendHaptics(SGCore::Haptics::SG_FFBCmd(ff[0], ff[1], ff[2], ff[3], ff[4]));

	return true;
}

/* sendThumperRaw is only for SenseGlove devices */
bool ASenseGloveController::sendThumperRaw(TEnumAsByte<ThumperWaveForm> wave)
{
	if (!SGCore::DeviceList::SenseCommRunning()) {
		UE_LOG(SenseGlove, Warning, TEXT("SenseComm is not running"));
		return false;
	}

	if (!controllerHandleNew || controllerHandleNew->GetDeviceType() != SGCore::DeviceType::SENSEGLOVE)
		return false;
	
	controllerHandleNew->SendHaptics(SGCore::Haptics::SG_ThumperCmd((int)wave.GetValue()));

	return true;
}

bool ASenseGloveController::activateVibroTactileFeedback(TEnumAsByte<SenseGloveHapticChannel> Actuator, float Duration, int Intensity, bool isLooping)
{
	
	int channel = (int) Actuator.GetValue();
	
	if (channel >= maxVibroChannels)
		return false;

	vibroActuators[channel].duration = Duration;
	vibroActuators[channel].currentValue = Intensity;
	vibroActuators[channel].isLooping = isLooping;
	vibroActuators[channel].bNeedsUpdate = true;

	return true;
}


bool ASenseGloveController::activateForceFeedback(TEnumAsByte<SenseGloveHapticChannel> Actuator, float Duration, int Intensity, bool isLooping)
{
	int channel = (int)Actuator.GetValue();

	if (channel >= maxFFChannels)
		return false;

	ffActuators[channel].duration = Duration;
	ffActuators[channel].currentValue = Intensity;
	ffActuators[channel].isLooping = isLooping;
	ffActuators[channel].bNeedsUpdate = true;

	return true;
}

bool ASenseGloveController::startCalibration() 
{
	if(!controllerHandleNew)
		return false;

	controllerHandleNew->ResetCalibrationRange();

	return true;
}

bool ASenseGloveController::stopCalibration() 
{
	if (!controllerHandleNew)
		return false;

	controllerHandleNew->UpdateCalibration(handProfile);
	return true;
}

void ASenseGloveController::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{

	if (!bDefaultPhysics) /* Default physics off*/
		return;

	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		USenseGloveCollider* collider = Cast<USenseGloveCollider>(HitComp);
		if (collider) {
			
			auto& colliderChannels = collider->assignedActuators;
			float duration = collider->staticHitDuration;
			int intensity = collider->hitIntensity;
			
			for (auto& channel: colliderChannels) {
				activateVibroTactileFeedback(channel, duration, intensity, false);
				if (GEngine && bEnableDebug) {
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::Printf(TEXT("HIT -- channel: %d intensity: %d, duration: %.2f"), (int) channel.GetValue(), intensity, duration));
				}
			}

		}

	}
	

}

void ASenseGloveController::OnOverlapEnter(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	UBoxComponent* box = Cast<UBoxComponent>(OverlappedComponent);
	USenseGloveCollider * col = Cast<USenseGloveCollider>(OtherComp);

	if(col) {
		//UE_LOG(SenseGlove, Warning, TEXT("SenseGlove Collider Box!"));
		return;
	}

	if ((OtherComp != nullptr) && (box != nullptr)) {
		FString name = box->GetName(); /* A bit hacky, but allows us to use one function */
		if (name == TEXT("ThumbGrabArea")) {
			tmpThumbComp = OtherComp;
		}
		else if (name == TEXT("IndexGrabArea")) {
			tmpIndexComp = OtherComp;
		}

		if ((tmpIndexComp == tmpThumbComp) && bGrabbingGesture) {
			FAttachmentTransformRules attachRule(EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, true);
			grabComp = tmpThumbComp;
			grabComp->AttachToComponent(thumbGrabArea, attachRule);
			isGrabbing = true;
			activateForceFeedback(SenseGloveHapticChannel::INDEX, 10.f, 100, true);
			activateForceFeedback(SenseGloveHapticChannel::THUMB, 10.f, 100, true);
			//UE_LOG(SenseGlove, Warning, TEXT("Attaching !"));
		}
	}


}

void ASenseGloveController::OnOverlapExit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	UBoxComponent* box = Cast<UBoxComponent>(OverlappedComponent);

	if ((OtherComp != nullptr) && (box != nullptr)) {
		FString name = box->GetName(); /* A bit hacky, but allows us to use one function */
		if (name == TEXT("ThumbGrabArea")) {
			tmpThumbComp = nullptr;
		}
		else if (name == TEXT("IndexGrabArea")) {
			tmpIndexComp = nullptr;
		}

		//releaseObjects();
	}
}

void ASenseGloveController::initializeGrabbing()
{
	thumbGrabArea->SetVisibility(true);
	indexGrabArea->SetVisibility(true);
	thumbGrabArea->OnComponentBeginOverlap.AddDynamic(this, &ASenseGloveController::OnOverlapEnter);
	thumbGrabArea->OnComponentEndOverlap.AddDynamic(this, &ASenseGloveController::OnOverlapExit);
	indexGrabArea->OnComponentBeginOverlap.AddDynamic(this, &ASenseGloveController::OnOverlapEnter);
	indexGrabArea->OnComponentEndOverlap.AddDynamic(this, &ASenseGloveController::OnOverlapExit);
}

void ASenseGloveController::deInitializeGrabbing() {
	
	thumbGrabArea->SetVisibility(false);
	indexGrabArea->SetVisibility(false);
	thumbGrabArea->OnComponentBeginOverlap.RemoveDynamic(this, &ASenseGloveController::OnOverlapEnter);
	thumbGrabArea->OnComponentEndOverlap.RemoveDynamic(this, &ASenseGloveController::OnOverlapExit);
	indexGrabArea->OnComponentBeginOverlap.RemoveDynamic(this, &ASenseGloveController::OnOverlapEnter);
	indexGrabArea->OnComponentEndOverlap.RemoveDynamic(this, &ASenseGloveController::OnOverlapExit);
}

void ASenseGloveController::initializePhysics()
{
	thumbCapsule->BindToActuator(SenseGloveHapticChannel::THUMB);
	thumbCapsule->OnComponentHit.AddDynamic(this, &ASenseGloveController::OnHit);

	indexCapsule->BindToActuator(SenseGloveHapticChannel::INDEX);
	indexCapsule->OnComponentHit.AddDynamic(this, &ASenseGloveController::OnHit);

	middleCapsule->BindToActuator(SenseGloveHapticChannel::MIDDLE);
	middleCapsule->OnComponentHit.AddDynamic(this, &ASenseGloveController::OnHit);

	ringCapsule->BindToActuator(SenseGloveHapticChannel::RING);
	ringCapsule->OnComponentHit.AddDynamic(this, &ASenseGloveController::OnHit);

}

void ASenseGloveController::deInitializePhysics()
{
	thumbCapsule->SetVisibility(false);	
	thumbCapsule->SetCollisionProfileName(FName("NoCollision"));
	thumbCapsule->OnComponentHit.RemoveDynamic(this, &ASenseGloveController::OnHit);

	indexCapsule->SetVisibility(false);
	indexCapsule->SetCollisionProfileName(FName("NoCollision"));
	indexCapsule->OnComponentHit.RemoveDynamic(this, &ASenseGloveController::OnHit);

	middleCapsule->SetVisibility(false);
	middleCapsule->SetCollisionProfileName(FName("NoCollision"));
	middleCapsule->OnComponentHit.RemoveDynamic(this, &ASenseGloveController::OnHit);

	ringCapsule->SetVisibility(false);
	ringCapsule->SetCollisionProfileName(FName("NoCollision"));
	ringCapsule->OnComponentHit.RemoveDynamic(this, &ASenseGloveController::OnHit);

}


void ASenseGloveController::initializeMeshContainer() {

	if (meshContainer->SkeletalMesh) {

		if (thumbCapsule->assignedBone == FName("None")) {
			thumbCapsule->assignedBone = FName("thumb_03_end");
		}
		thumbCapsule->BindToSkelet(meshContainer);

		if (indexCapsule->assignedBone == FName("None")) {
			indexCapsule->assignedBone = FName("f_index_03_end");
		}
		indexCapsule->BindToSkelet(meshContainer);

		if (middleCapsule->assignedBone == FName("None")) {
			middleCapsule->assignedBone = FName("f_middle_03_end");
		}
		middleCapsule->BindToSkelet(meshContainer);

		if (ringCapsule->assignedBone == FName("None")) {
			ringCapsule->assignedBone = FName("f_ring_03_end");
		}
		ringCapsule->BindToSkelet(meshContainer);

		FAttachmentTransformRules attachRule(EAttachmentRule::SnapToTarget, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, true);
		thumbGrabArea->AttachToComponent(meshContainer, attachRule, FName("thumb_03_end"));
		indexGrabArea->AttachToComponent(meshContainer, attachRule, FName("f_index_03_end"));


	}
}

void ASenseGloveController::releaseObjects()
{
	FDetachmentTransformRules detachRule(EDetachmentRule::KeepWorld, EDetachmentRule::KeepWorld, EDetachmentRule::KeepWorld, true);

	if (isGrabbing) {
		if (grabComp != nullptr) {
			grabComp->DetachFromComponent(detachRule);
			activateForceFeedback(SenseGloveHapticChannel::INDEX, 0.f, 0, false);
			activateForceFeedback(SenseGloveHapticChannel::THUMB, 0.f, 0, false);
			grabComp->SetSimulatePhysics(true);
			isGrabbing = false;
		}
		//UE_LOG(SenseGlove, Warning, TEXT("Releasing!"));
	}

}
