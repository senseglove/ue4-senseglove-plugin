/* robin@senseglove.com */

#include "SenseGloveCollider.h"

USenseGloveCollider::USenseGloveCollider() {

	CapsuleRadius = 1.0f;
	CapsuleHalfHeight = 2.5f;
	this->AddLocalRotation(FRotator(0, 0, 90));
	this->SetEnableGravity(false);
	this->SetCollisionProfileName(FName("BlockAll"));
	this->SetNotifyRigidBodyCollision(true);
}

void USenseGloveCollider::BindToActuator(TEnumAsByte<SenseGloveHapticChannel> actuator)
{
	assignedActuators.Add(actuator);
}

void USenseGloveCollider::BindToSkelet(USkeletalMeshComponent * meshContainer)
{

	if (meshContainer->GetBoneIndex(assignedBone) == INDEX_NONE) {

		UE_LOG(SenseGlove, Warning, TEXT("Cannot find bone name, not binding!"));
		return;
	}

	FAttachmentTransformRules attachRule(EAttachmentRule::SnapToTarget, EAttachmentRule::KeepRelative, EAttachmentRule::KeepWorld, true);
	this->AttachToComponent(meshContainer, attachRule, assignedBone);
	
}
