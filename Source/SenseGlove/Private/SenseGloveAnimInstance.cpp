/* robin@senseglove.com */
#include "SenseGloveAnimInstance.h"

#define LOCTEXT_NAMESPACE "FSenseGloveModule"

/* Function that can modify the update speed */
void FSGProxy::Update(float DeltaSeconds) {
	
	/* Quick and dirty HACK to check if hand is grabbing or not */
	if (hai1.y < 5.f) {
		if (Controller->bGrabbingGesture) {
			Controller->bGrabbingGesture = false;
			Controller->bNeedsRelease = true;
		}
	}
	else {
		if (!Controller->bGrabbingGesture)
			Controller->bGrabbingGesture = true;
	}

}

bool FSGProxy::Evaluate(FPoseContext& Output)
{

	/* Check if SenseComm is opened */
	if (!SGCore::DeviceList::SenseCommRunning()) {
		UE_LOG(SenseGlove, Warning, TEXT("SenseCom is not running! Please run SenseCom"));
		return false;
	}
	
	/* Check if there exists a USenseGloveController Component in the hierarchy */
	if (!Controller || !Controller->controllerHandleNew) {
		return false;
	}

	SGCore::HandPose handPose;

	/* Below is explicit for debugging purposes */
	if (Controller->controllerHandleNew->GetHandPose(Controller->handProfile, handPose)) {
	
			handAngles = handPose.handAngles;

			hai1 = SGCore::Kinematics::Values::Degrees(handAngles[1][0]);
			hai2 = SGCore::Kinematics::Values::Degrees(handAngles[1][1]);
			hai3 = SGCore::Kinematics::Values::Degrees(handAngles[1][2]);

			ham1 = SGCore::Kinematics::Values::Degrees(handAngles[2][0]);
			ham2 = SGCore::Kinematics::Values::Degrees(handAngles[2][1]);
			ham3 = SGCore::Kinematics::Values::Degrees(handAngles[2][2]);

			har1 = SGCore::Kinematics::Values::Degrees(handAngles[3][0]);
			har2 = SGCore::Kinematics::Values::Degrees(handAngles[3][1]);
			har3 = SGCore::Kinematics::Values::Degrees(handAngles[3][2]);

			hap1 = SGCore::Kinematics::Values::Degrees(handAngles[4][0]);
			hap2 = SGCore::Kinematics::Values::Degrees(handAngles[4][1]);
			hap3 = SGCore::Kinematics::Values::Degrees(handAngles[4][2]);

			hat1 = SGCore::Kinematics::Values::Degrees(handAngles[0][0]);
			hat2 = SGCore::Kinematics::Values::Degrees(handAngles[0][1]);
			hat3 = SGCore::Kinematics::Values::Degrees(handAngles[0][2]);

	}
	
	/* HACK: set initial z-value first phalanx to 0 for Nova devices */
	if (Controller->controllerHandleNew->GetDeviceType() == SGCore::DeviceType::NOVA) {	
			hai1.z = 0;
			ham1.z = 0;
			har1.z = 0;
			hap1.z = 0;
	}

	int corr = 1;
	if (!Controller->rightHanded)
		corr = -1;

	FQuat adji1(FRotator(-hai1.x, -hai1.y, corr * -hai1.z));
	FQuat adji2(FRotator(-hai2.x, -hai2.y, corr * -hai2.z));
	FQuat adji3(FRotator(-hai3.x, -hai3.y, corr * -hai3.z));

	FQuat adjm1(FRotator(-ham1.x, -ham1.y, corr * -ham1.z));
	FQuat adjm2(FRotator(-ham2.x, -ham2.y, corr * -ham2.z));
	FQuat adjm3(FRotator(-ham3.x, -ham3.y, corr * -ham3.z));

	FQuat adjr1(FRotator(-har1.x, -har1.y, corr * -har1.z));
	FQuat adjr2(FRotator(-har2.x, -har2.y, corr * -har2.z));
	FQuat adjr3(FRotator(-har3.x, -har3.y, corr * -har3.z));

	FQuat adjp1(FRotator(-hap1.x, -hap1.y, corr * -hap1.z));
	FQuat adjp2(FRotator(-hap2.x, -hap2.y, corr * -hap2.z));
	FQuat adjp3(FRotator(-hap3.x, -hap3.y, corr * -hap3.z));

	FQuat adjt1;

	/* HACK: The -15 degrees is an adjustment for the current hand Model */
	if (corr == 1)
		adjt1 = FQuat(FRotator(hat1.x, -hat1.z - 15, hat1.y));
	else {
		adjt1 = FQuat(FRotator(hat1.x, hat1.z - 15, hat1.y));
	}

	FQuat adjt2(FRotator(0, 0, hat2.y));
	FQuat adjt3(FRotator(0, 0, hat3.y));

	
	/* Use the reference pose, to set an initial position and then apply rotations */
	Output.ResetToRefPose();

	Output.Pose[(FCompactPoseBoneIndex)i1].SetRotation(adji1);
	Output.Pose[(FCompactPoseBoneIndex)i2].SetRotation(adji2);
	Output.Pose[(FCompactPoseBoneIndex)i3].SetRotation(adji3);

	Output.Pose[(FCompactPoseBoneIndex)m1].SetRotation(adjm1);
	Output.Pose[(FCompactPoseBoneIndex)m2].SetRotation(adjm2);
	Output.Pose[(FCompactPoseBoneIndex)m3].SetRotation(adjm3);

	Output.Pose[(FCompactPoseBoneIndex)r1].SetRotation(adjr1);
	Output.Pose[(FCompactPoseBoneIndex)r2].SetRotation(adjr2);
	Output.Pose[(FCompactPoseBoneIndex)r3].SetRotation(adjr3);

	Output.Pose[(FCompactPoseBoneIndex)p1].SetRotation(adjp1);
	Output.Pose[(FCompactPoseBoneIndex)p2].SetRotation(adjp2);
	Output.Pose[(FCompactPoseBoneIndex)p3].SetRotation(adjp3);

	Output.Pose[(FCompactPoseBoneIndex)t1].SetRotation(adjt1);	
	Output.Pose[(FCompactPoseBoneIndex)t2].SetRotation(adjt2);
	Output.Pose[(FCompactPoseBoneIndex)t3].SetRotation(adjt3);

	return true;
}
/* First time the instance is spawned (or opened in the editor)
Thise function gets called many times when previewing, opening, dragging dropping, simulating */
void USenseGloveAnimInstance::NativeInitializeAnimation()
{

	AActor* actorContainer = GetOwningActor();
	ASenseGloveController* owningActor = Cast<ASenseGloveController>(actorContainer);

	if (!owningActor) {
		UE_LOG(SenseGlove, Warning, TEXT("Can't find owning actor object!"));
	}
	else {
		/* Get the first available SenseGlove controller */
		Proxy.Controller = owningActor;

		if (Proxy.Controller) {
			UE_LOG(SenseGlove, Log, TEXT("Found controller component"));
			USkeletalMeshComponent* skeletContainer = this->GetSkelMeshComponent();
			
			if (skeletContainer->GetBoneIndex(FName("metarig")) == INDEX_NONE) {
				UE_LOG(SenseGlove, Log, TEXT("Incompatible skeleton detected"));
			}
				
			Proxy.t1 = skeletContainer->GetBoneIndex(FName("thumb_01"));
			Proxy.t2 = skeletContainer->GetBoneIndex(FName("thumb_02"));
			Proxy.t3 = skeletContainer->GetBoneIndex(FName("thumb_03"));

			Proxy.i1 = skeletContainer->GetBoneIndex(FName("f_index_01"));
			Proxy.i2 = skeletContainer->GetBoneIndex(FName("f_index_02"));
			Proxy.i3 = skeletContainer->GetBoneIndex(FName("f_index_03"));

			Proxy.m1 = skeletContainer->GetBoneIndex(FName("f_middle_01"));
			Proxy.m2 = skeletContainer->GetBoneIndex(FName("f_middle_02"));
			Proxy.m3 = skeletContainer->GetBoneIndex(FName("f_middle_03"));

			Proxy.r1 = skeletContainer->GetBoneIndex(FName("f_ring_01"));
			Proxy.r2 = skeletContainer->GetBoneIndex(FName("f_ring_02"));
			Proxy.r3 = skeletContainer->GetBoneIndex(FName("f_ring_03"));

			Proxy.p1 = skeletContainer->GetBoneIndex(FName("f_pinky_01"));
			Proxy.p2 = skeletContainer->GetBoneIndex(FName("f_pinky_02"));
			Proxy.p3 = skeletContainer->GetBoneIndex(FName("f_pinky_03"));
		}

	}

}

void USenseGloveAnimInstance::NativeUninitializeAnimation() {

	AActor* actorContainer = GetOwningActor();
	ASenseGloveController* owningActor = Cast<ASenseGloveController>(actorContainer);

	if (!owningActor) {
		UE_LOG(SenseGlove, Warning, TEXT("Can't find owning actor object!"));
	}
	else { /* Make sure to terminate all haptic feedback channels */
		if (owningActor->controllerHandleNew) {
			owningActor->sendBuzzRaw(0, 0, 0, 0, 0, 0);
			owningActor->sendForceFeedbackRaw(0, 0, 0, 0, 0);
		}
	}

}

#undef LOCTEXT_NAMESPACE
