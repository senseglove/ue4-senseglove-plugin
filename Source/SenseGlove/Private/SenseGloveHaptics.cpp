// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SenseGloveHaptics.h"
#include "Misc/App.h"

USenseGloveHaptics::USenseGloveHaptics(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Make sure that by default the force feedback effect has an entry
	//FForceFeedbackChannelDetails ChannelDetail;
	//ChannelDetails.Add(ChannelDetail);
}

#if WITH_EDITOR
void USenseGloveHaptics::PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent)
{
	// After any edit (really we only care about the curve, but easier this way) update the cached duration value
	//GetDuration();
}

#endif

float USenseGloveHaptics::GetDuration()
{
	return 1.0f;
}

void USenseGloveHaptics::GetValues() const
{

}

/* returns true if actuator needs to be fired (updated)
 false otherwise */

bool FSenseGloveHapticActuator::Update(const float DeltaTime)
{
	if (this->bNeedsUpdate) { /* initial Fire */
		this->bNeedsUpdate = false;
		return true;
	}

	if (this->duration <= 0 || this->isLooping) /* looping will ignore deltatime */
		return false;

	this->duration -= DeltaTime;

	if (this->duration <= 0) {
		this->currentValue = 0;
		return true;
	}

	return false;

}
